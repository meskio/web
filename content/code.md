---
title: "Code"
date: 2020-11-23T09:59:09+01:00
images:
  - /img/hacking.jpg
---

### [leap](https://0xacab.org/leap/)

VPN platform used by [riseup](https://riseup.net/vpn/), [calyx](https://calyxinstitute.org/) and others.

### [lowry](https://git.sindominio.net/sindominio/lowry/)

Account management system for SinDominio, built on top of LDAP.

### [cicer](https://0xacab.org/meskio/cicer/)

A stock and orders management system for a consumers cooperative.

### [almanac](https://crates.io/crates/almanac)

Simple .ics parser to pretty print the events on the terminal.


## Old projects

### [tudu](http://code.meskio.net/tudu/)

A comand line interface to manage hierarchical todos.

### [emma](https://gitlab.com/emma-bot/)

An extensible bot for digital assembly written in python.

### [lectern](https://gitlab.com/lectern/lectern/)

A command line epub reader.
