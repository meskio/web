---
date: 2011-11-04 00:37:25+00:00
title: Postapocalipsis en el supermercado
tags:
- spanish
- cyberpunk
- hack
- hackmeeting
- hacktivismo
- postapocalipsis
- supermercado
---

{{< figure src="/img/2011/hm-posapoca.jpg" alt="postapocalipsis en el hackmeeting" class="right" >}}
¡Ciber-Hermanas y tecno-hermanos, que habéis sucumbido al consumismo!
Ha llegado la hora de que los oprimidos os levantéis y recuperéis el control sobre vuestras vidas perdidas [para encontrarlas buscadlas en Google, que os conoce mejor que vosotr@s mism@s].

Venimos hoy aquí hermanas, celebremos en este tiempo presente el futuro en el que un día este lugar serán ruinas.

El estruendoso silencio de este templo de consumo y triste luz fluorescente será un día silencio y oscuridad. Pero en la noche se rompera por los deliciosos graves de raves tecno-hardcore y queer-core-crust parties.

Y ese día el mundo será el día más hermoso de vuestras vidas. Recemos hermanas, recemos a root (todos: ¡compilado sea su nombre!).

Arrepentíos de haber rendido vuestras vidas a Facebook, porque su fin está cerca. El infonegocio morirá, desapareciendo vuestras memorias con él. Y entonces será el llanto y se producirá el crujir de dientes (¡¿dónde están las fotos de mi nieto?!)

Pero todavía estáis a tiempo de redimiros. El fin todavía no ha llegado. Caed del caballo y oid el silencio, degustad el vacío y ved la oscuridad: Lorea es misericordiosa y os acogerá en su seno, oh hijas pródigas, soy piadoso. Confesad vuestras culpas, y cabalgad al gnu en penitencia.

Bienaventuradas aquellas que confíen sus datos a Diaspora, porque sus credenciales serán federadas. En verdad os digo, identica proporciona sopas ondeadas a Twitter, y respeta vuestra privacidad.

Este es el día en el que el sol se oculta tras la montaña, las nubes ensombrecen la llanura y las vacas rechinan de sorpresa. Tolón, tolón. Llegará el día en el que estaréis agradecidas. Comed y bebed de este código fuente, de cuyas lineas os alimentareis.

¡Renunciad a los binarios privativos! Un nuevo mandamiento os doy: amad a Debian sobre todas las cosas, y a los interpretes y compiladores como a vosotras mismas (apt, apt!)

Subleavaos Hermanas. Vuestras mascotas os odian. Abandonad a vuestros maridos. Romped vuestros grilletes. Dejad vuestros tristes trabajos de títeres. Olvidad vuestros hobbies. Basta ya de aceptar vuestras frustraciones. Rebelaos. El futuro comienza hoy.

[https://hackmeeting.org](https://hackmeeting.org)
