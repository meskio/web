---
date: 2011-11-30 15:14:47+00:00
title: Sobre hombres y máquinas...
tags:
- quote
- spanish
- hombre
- máquina
- soñar
- turing
---

{{< figure src="/img/2011/steampunk.jpg" alt="steampunk by PReally" class="right" >}}
> Hubo un tiempo en el que los hombres soñaron en que algún día las máquinas serían capaces de calcular y pensar como ellos lo hacía...
> 
> Hubo un tiempo en el que hombres y máquinas emprendieron el mismo camino...
> 
> Hubo un tiempo en el que las máquinas soñaron que algún día serían capaces de calcular y pensar como lo hacían los hombres...
> 
> Pero hubo un tiempo en el que las máquinas soñaron en que algún día otras máquinas serían capaces de calcular y pensar como ellas lo hacían...
> 
> Hubo un tiempo en el que hombres y máquinas creyeron estar creados a imagen y semejanza de un mismo dios...
> 
> Hubo un tiempo...


Rafael Lahoz-Beltrá - Turing: Del primer ordenador a la inteligencia artificial
