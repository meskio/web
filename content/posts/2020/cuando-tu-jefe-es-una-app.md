---
title: "Cuando Tu Jefe Es Una App"
date: 2020-11-30T12:53:14+01:00
tags: 
  - spanish
  - review
  - book
  - smartification
  - capitalismo de plataforma
  - crowdsourcing
  - quote
---

{{< figure src="/img/2020/cuando-tu-jefe-es-una-app.png" alt="cuando to jefe es una app" class="right" >}}

[Cuando tu jefe es una 
app](https://www.katakrak.net/cas/editorial/libro/cuando-tu-jefe-es-una-app) 
hace un análisis desde la academia y los estudios sociológicos de la situación 
laboral dentro del "capitalismo de plataforma" (como uber, etsy, mechanical turk 
o deliberoo). El libro es principalmente un recopilatorio de artículos de 
investigación sin terminar de sacar una linea argumental conexa. Aunque con 
muchas reflexiones interesantes sobre el entorno de trabajo de las plataformas y 
de como lo viven las que participan de el. Bastante centrado en el entorno 
frances, del que vienen las investigadoras que lo escriben.

Empieza dibujando la separación entre las plataformas y la "Economía Social y 
Solidaria" (ESS), dentro de la que algunas plataformas intentan mostrarse como 
parte. Planteando como las plataformas producen un consumismo y despilfarro 
aunque se dibujen como una alternativa "ecológica". Pero reconociendo algunas 
realidades cercanas:

> El capitalismo de plataformas y la ESS comparten el hecho de proponer a 
> personas que buscan alternativas profesionales actividades precarias cuya 
> naturaleza laboral suele negarse.

Hace un análisis sobre la "mercantilización del tiempo perdido" y las 
plataformas de *crowdsourcing*, como etsy donde puedes vender tus creaciones 
artesanales. Donde la gran mayoría de las que venden ahí lo hacen como una 
afición o un suplemento a su actividad central. Creo que aquí me hubiera gustado 
un análisis mas a fondo del tema, mirando la expansión del capitalismo a nuevos 
territorios mercantilizando nuestras pasiones y construyendo una cultura en la 
que nos sintamos obligadas a seguir produciendo dinero en nuestro "tiempo 
libre". Pero aún así deja reflexiones interesantes al respecto:

> la extensión del trabajo producido por la economía digital tiene menos que ver 
> con la creación de nuevos empleos y con la democratización del mercado de 
> trabajo que con la transformación del contenido mismo del trabajo y con su 
> intensificación.

Una parte del libro lo dedican a estudiar las respuestas en forma de 
movilizaciones o jurídicas de las "trabajadoras" de plataforma. Y mostrar como 
aún siendo uno de los entornos actualmente mas precarizados es uno de los que 
menor penetración tienen los sindicatos y como las propias plataformas se 
organizan para que las trabajadoras se vean como competidoras.

Con un titulo tan evocador, la verdad es que me esperaba que no se quedara solo 
en una visión laboral mas clásica y que entrara a analizar como en estas 
plataformas tu jefe es un algoritmo. Donde tu actividad esta monitorizada y 
cuestionada por este algoritmo, que de forma automática decide contratarte o 
despedirte o darte mas o menos prioridad para conseguir trabajos.
