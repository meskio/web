---
title: "El Internet de las Personas"
date: 2020-12-01T00:00:00+01:00
toc: false
tags: 
  - arainfo
  - smartification
  - spanish
  - amazon
  - 5G
  - IoT
---

{{< figure src="/img/2020/wrongamazon.png" alt="the wrong amazon is burning" class="right" >}}

El pasado miércoles las aspiradoras dejaron de limpiar, los timbres de las casas 
dejaron de sonar, las luces dejaron de encenderse, dispositivos *smart* en todos 
los lugares del globo se volvían rebeldes.  Por un momento parecía que 
hubiéramos llegado a ese momento anunciado donde las maquinas se revelan contra 
sus dueños, pero por ahora ["solo" es un problema en 
AWS](https://www.xataka.com/otros-dispositivos/caida-parcial-servidores-amazon-ha-provocado-que-aspiradoras-dejen-funcionar).

AWS es la *nube* de amazon, donde se alojan un tercio de los servicios de la 
*nube*. Ese [nuevo 
vecino](https://arainfo.org/adivina-quien-es-el-nuevo-vecino/) que siguiendo la 
tradición [Aragonesa de 
pelotazos](https://arainfo.org/de-nieve-cerdos-y-big-data-memorias-del-subdesarrollo-en-pleno-colapso/) 
se esta instalando en nuestro territorio. Que se deshincha por momentos dejando 
atrás la retorica de miles de puestos de trabajo para hablar de [un máximo de 60 
empleados](https://www.heraldo.es/noticias/aragon/huesca/2020/08/10/amazon-abrira-en-huesca-a-finales-de-2021-pero-supedita-las-otras-dos-fases-a-la-demanda-del-mercado-1389943.html) 
en los próximos 10 años en cada uno de los 3 centro de datos que va a construir 
en Aragón. Pero no caigamos en su trampa de evaluar las cosas desde una visión 
económica y de puestos de trabajo, y reflexionemos sobre que sociedad (e 
internet) queremos construir.

Justo estos días esta circulando la campaña 
[MakeAmazonPay](https://makeamazonpay.com/es/) donde se le exige a amazon 
condiciones laborales dignas, sostenibilidad ecológica y que pague sus impuestos 
en los países donde realiza sus actividades. Es un primer paso, pero igual 
deberíamos aspirar a mas, y desmontar las grandes corporaciones como amazon en 
búsqueda de consumo de cercanía de pequeño comercio, ya sea en nuestras compras 
como en donde se alojan los datos en la red.

Volviendo a las aspiradoras y timbres, lo que pasó el pasado miércoles fue que 
los centros de datos de AWS en el Norte de Virginia (EE. UU.) sufrieron un 
apagón.  Todos estos dispositivos *smart* se conectan con servidores en estos 
centros de datos y al no poder acceder a ellos dejaron de funcionar.  Internet 
cada vez esta mas centralizada, concentrando todos los datos en unos pocos 
lugares (y manos). AWS es con diferencia donde mas servicios se concentran 
haciendo que cuando tiene problemas afecte a grandes volúmenes de la población, 
como vimos el pasado miércoles.

Pero la duda que no me quito de la cabeza es: ¿de verdad necesita mi aspiradora 
tener internet para limpiar mi salón? ¿por que si vienen visitas mi timbre tiene 
que informarle a su fabricante? ¿para que tiene que comunicarse la bombilla de 
mi mesilla con un servidor en la otra punta del mundo si quiero encenderla?

Estos dispositivos son los que la industria llama el "Internet de las Cosas".  
Dibujan ese futuro al que estamos entrando a pasos agigantados donde las cosas 
que nos rodean están conectadas a internet y *hablan* entre ellas a nuestras 
espaldas. Lo vemos con las Alexas entrando en nuestras casas como pequeños 
altavoces que escuchan todo lo que decimos y están listos para cumplir nuestros 
deseos mas banales encendiendo las luces, bajando las persianas o subiendo la 
calefacción. Comunicándose con nuestra *smart* bombilla, *smart* persiana o 
*smart* termostato.

Estas cosas conectadas no se centran solo en el interior de nuestras casas.  
Vemos como poco a poco van invadiendo nuestros barrios como los contadores 
eléctricos que esta instalando Endesa con la escusa de reducir el consumo 
eléctrico, que me expliquen como se reduce el consumo introduciendo millones de 
aparatos que consumen electricidad. O los sensores en los contenedores de basura 
que hay en ciudades como Barcelona, que detectan cuando están llenos y 
"optimizan" la recogida de basura. Pero en muchos casos están descubriendo que 
las rutas de recogida existentes ya eran las mejores y las ciudades se han 
gastado millonadas en poner aparatos con pilas que no se pueden cambiar en todos 
los contenedores que a los pocos años hay que tirarlos y reemplazar por otro.

Cuando las corporaciones se sientan a diseñar el futuro de internet, en su forma 
de telefonía móvil proponen estándares como el 5G. Que pone a las [cosas como el 
eje central](https://www.rhizomatica.org/hablando-de-mi-5ta-generacion/) de las 
comunicaciones relegando a las personas a actores secundarios de esta red. Lo 
vemos en como el 5G prioriza el crear una hiperconectividad en las zonas con 
mayor densidad de población, aumentando la velocidad pero principalmente el 
numero de dispositivos que pueden estar conectados por metro cuadrado. Lo cual 
no es una necesidad de conectividad humana, sino solo necesario si nuestro 
futuro implican centenares de cosas conectadas a nuestro alrededor.

Como parte del desarrollo de 5G la industria esta creando una serie de 
protocolos y demostraciones de uso de esta nueva tecnología. Uno de ellos es el 
protocolo de comunicación coche-coche y coche-peaton. Donde los futuros coches 
podrán interactuar entre ellos, para no chocarse, y los peatones tendrán que 
llevar dispositivos que puedan hablar con estos coches para no ser atropellados. 
Vemos que el patrón de cesión de espacio al coche no solo se da en las ciudades 
sino también en internet.

El 5G prioriza esta conectividad en las ciudades, dejando de lado el resolver 
problemas reales del internet actual. Como el acceso a internet en zonas 
rurales, que no solo no pretende mejorar sino que [probablemente lo 
empeore](https://www.rhizomatica.org/la-tecnologia-5g-no-reducira-la-brecha-digital-y-podria-incluso-empeorarla/).  
O el cambio climático, donde el 5G plantea poner antenas en cada poste de la luz 
de las ciudades, aumentando enormemente los recursos necesarios y los deshechos 
producidos, y se calcula que cada torre de 5G va a consumir entre [2 y 3 veces 
mas electricidad que las de 
4G](https://www.fiercewireless.com/tech/5g-base-stations-use-a-lot-more-energy-than-4g-base-stations-says-mtn).

Es hora de plantearnos como sería un internet de las personas. Dejando atrás la 
visión capitalista con su necesidad de expansión y de caducar las tecnologías. 
Donde el desarrollo no sea de la tecnología por la tecnología. Igual no 
necesitamos que todas las cosas que nos rodean tengan un chip y estén conectadas 
a internet. Un internet de las personas donde la tecnología la evaluemos según 
nuestras necesidades humanas.
