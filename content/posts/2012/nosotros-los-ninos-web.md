---
date: 2012-03-08 09:20:38+00:00
title: Nosotros, los niños web
tags:
- spanish
- quotes
- acta
- polonia
- web
---

{{< figure src="/img/2012/anonymous.jpg" alt="Anonymous World Web War I by muusedesign d4nger2" class="right" >}}
> **Crecimos con el Internet y en Internet.** Esto es lo que nos hace diferentes, lo que hace que la crucial, aunque sorprendente desde su punto de vista, diferencia: nosotros no *“surfeamos”* el Internet, para nosotros no es un “lugar” o “espacio virtual”. El Internet para nosotros no es algo externo a la realidad, sino una parte de ella: una capa invisible, pero presente constantemente y entrelazada con el entorno físico. Nosotros no usamos el Internet, vivimos en el Internet y a través de. Si tuviéramos que contarles nuestro *bildnungsroman*, el análogo, se podría decir que hubo un aspecto natural al Internet en cada experiencia que nos ha formado. Nos hicimos amigos y enemigos en línea, preparamos aplicación para la cuna en línea, planeamos fiestas y sesiones de estudio de línea, nos enamoramos y rompimos en línea. La web para nosotros no es una tecnología que teníamos que aprender y que nos las arreglamos para entenderla. **La Web es un proceso para nosotros**, que sucede y se transforma continuamente ante nuestros ojos, con nosotros y a través de nosotros. Las tecnologías aparecen y luego se disuelven en las periferias, los sitios web están construidos, florecen y luego se van, pero la web sigue, porque nosotros somos la Web; nosotros, comunicándonos con los otros de una forma que es natural para nosotros, más intensa y más eficiente que nunca antes en la historia de la humanidad.
> 
> ...
> 
> Para nosotros, **la Web es una especie de memoria externa compartida.** No tenemos que recordar los detalles innecesarios: fechas, cantidades, fórmulas, oraciones, nombres de calles, definiciones detalladas. Es suficiente para nosotros un resumen, la esencia de lo que se necesita para procesar la información y relacionarla con los demás. En caso de que necesitemos los detalles, podemos encontrarlos en cuestión de segundos. Del mismo modo, no tenemos que ser expertos en todo, porque sabemos dónde encontrar las personas que se especializan en lo que nosotros mismos no sabemos, y en quienes confiamos. Las personas que comparten su experiencia con nosotros sin fines de lucro, lo hacen debido a nuestra creencia compartida de que la información existe en el movimiento, de que quiere ser libre, de que todos nos beneficiamos del intercambio de información. Todos los días: estudiar, trabajar, resolver problemas cotidianos, perseguir nuestros intereses. Sabemos cómo competir y nos gusta hacerlo, pero **nuestra competencia, nuestro deseo de ser diferentes, se basa en el conocimiento, la habilidad para interpretar y procesar la información, y no en el monopolio de la misma.*


*[“My, dzieci sieci”](http://pokazywarka.pl/pm1pgl/) - [Piotr Czerski](https://twitter.com/#!/piotrczerski) ([texto traducido](http://alt1040.com/2012/02/nosotros-los-ninos-web))*
