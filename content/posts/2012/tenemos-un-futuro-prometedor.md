---
date: 2012-03-02 14:19:55+00:00
title: Tenemos un futuro prometedor
tags:
- spanish
- arainfo
- acta
- bittorrent
- blogs
- cultura libre
- emule
- facebook
- gran hermano
- guifi
- ley sinde
- megaupload
- p2p
- sopa
- the pirate bay
---

{{< figure src="/img/2012/ancient-area-network.png" alt="Ancient Area Network by eimer" class="right" >}}
Para internet el tiempo pasa muy deprisa, hace poco más de una década no sabíamos que era una página web, ahora ya no recordamos como era la vida antes de Facebook. En este tiempo hemos aprendido que la cultura no necesita estar confinada en soportes físicos, que si la sacamos de ellos crece y se reproduce, que todas somos participes de nuestra cultura creándola y disfrutándola.

Los blogs nos han ayudado a sacar nuestro lado periodístico y escritor. Nos hemos visto como fotógrafas publicando en galerías cibernéticas. En nuestras casas hemos creado canciones y vídeos que cientos de personas han descargado. Por pura diversión hemos tomado las obras de otros para remezclarlas y crear nuevas.

Desde Alejandría no habíamos visto una biblioteca tan completa. Aquel viejo sueño del acceso universal a la cultura parece hacerse realidad entre bits y chips. Nuestra serie favorita esta a un click de distancia junto con libros y música por escuchar.

Donde antes había una monocultura ahora vemos una explosión de gustos diferentes. El acceder a gran variedad de obras nos ha ayudado a descubrir nuestros gustos propios fuera de la presión mediática del “top ventas”.

Pero no todo el mundo está feliz con esta nueva situación. La industria que se formó vendiendo la cultura en soportes materiales no comprende este entorno en el que no cabe su modo de negocio. En su lucha por mantener un modelo que solo beneficia a una minoría ponen en peligro esta internet creativa. Crean leyes como la [Ley Sinde](http://es.wikipedia.org/wiki/Ley_de_Econom%C3%ADa_Sostenible), [dictada por Estados Unidos](http://cultura.elpais.com/cultura/2012/01/03/actualidad/1325545206_850215.html), o el [ACTA](http://es.wikipedia.org/wiki/Acuerdo_Comercial_Anti-Falsificaci%C3%B3n), negociado y firmado en secreto. De vez en cuando vemos pequeñas victorias como [el caso de la Ley SOPA](http://www.publico.es/culturas/417699/disminuye-el-apoyo-a-la-ley-sopa-tras-las-protestas-en-eeuu) estadounidense, retirada tras la masiva oposición de los internautas.

El gran cambio parece haberse dado con el [cierre de Megaupload](http://www.publico.es/culturas/417771/el-fbi-cierra-megaupload-el-mayor-servicio-de-descargas-directas). Por primera vez se cierra un servicio sin previo aviso y sin dar ninguna garantía a sus usuarios.

No hace tanto tiempo compartíamos cultura a través de redes p2p. ¿Recuerdas el eMule? ¿o el Bittorrent? Con la llegada de los servicios de descarga directa todo cambió, era mucho más fácil dejar la cultura en servidores comerciales que compartirla entre iguales. Perdimos el poder sobre nuestros ficheros delegándolos a compañías como Megaupload para que cuidaran de ellos.

El cierre de el mayor sitio de descarga directa, y el consecuente borrado de ficheros de otros servicios por miedo a que les pase lo mismo, nos brinda la oportunidad de recuperar el control sobre nuestros datos. En el poco tiempo que lleva Megaupload cerrado hemos visto como las redes p2p, especialmente el Bittorrent, vuelven a ser usadas masivamente. Los ficheros vuelven a estar en nuestros discos duros, la información no desaparece aunque cierren las principales páginas web que usamos.

Esta última semana hemos visto como [condenaban a los creadores de “the pirate bay”](http://www.publico.es/culturas/419815/la-sentencia-contra-los-fundadores-de-pirate-bay-es-firme-culpables), el mayor buscador de Bittorrents. Pero nuestras ansias de compartir no se ven paradas por muchas webs que cierren. Ya tenemos buscadores de contenido distribuidos para los que no necesitamos servidores que puedan cerrar.

Internet ha cambiado nuestra forma de entender la cultura, de accederla y de crearla. Nos queda todavía por ver muchos intentos de frenar esta explosión cultural, intentos de poner barreras a este cibermundo que se creó sin leyes ni gobiernos. Lo que todavía no han entendido los gobiernos y la industria es que ya han perdido la batalla, que una vez que hemos entendido que la cultura es nuestra ya no nos la podrán arrebatar. La cultura compartida solo puede ir a mas, como seguramente veamos en los próximos años con los libros gracias a la entrada del papel electrónico. Si hace falta construiremos otro internet, como ya están haciendo con [guifi.net](http://guifi.net/).

Tenemos un futuro prometedor, no dejemos que nos lo roben

*Escrito para [arainfo](https://arainfo.org/tenemos-un-futuro-prometedor-no-dejemos-que-nos-lo-roben/)*
