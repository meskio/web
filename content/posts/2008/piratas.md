---
date: 2008-06-25 23:21:00+00:00
title: Piratas
tags:
- spanish
- piracy
- history
---

Hemos visto muchas novelas y películas mostrando a los piratas como  hombres violentos y peligrosos. En realidad fueron bastante diferentes a  la imagen que tenemos de ellos.

Es verdad que hubo muchos barcos piratas y muchas formas diferentes  organización en ellos, pero en general los piratas del Caribe de los  siglos XVII y XVIII se regían por un estricto código de honor.

**Eran democráticos**. Los miembros de la tripulación se  enrolaban en una misión de forma voluntaria y elegían a su capitán de  forma democrática, por votación entre todos. En cualquier momento podían  reelegir a un nuevo capitán votando de nuevo. Las decisiones se tomaban  entre todos, solo en combate el capitán tenía un poder especial, pues  era el que tomaba las decisiones. Es verdad que muchas veces no se  decidían o cambiaban de rumbo muchas veces en pocos días, a veces la  toma de decisiones se complicaba.

**Eran igualitarios**. Todos los tesoros se  repartían entre todos en partes iguales, a excepción del capitán, que en  algunos barcos se le daba algo mas que a los demás. No veían  diferencias de raza o genero. Eran contrarios a la esclavitud, muchos  esclavos fueron liberados por ellos y aceptados como un pirata mas. Las  tripulaciones solían estar compuestas por un amalgama de culturas, con  gentes de todas partes del mundo. Tampoco veían distinción entre hombre y  mujeres, es verdad que la mayoría de los piratas fueron hombres, pero  hubo claros ejemplos de mujeres piratas, como por ejemplo Anne Bonny que  llego a ser capitana de un barco.

**Eran libres**. Vivían fuera de las leyes y de las  patrias, tal y como deseaban. Su barco y sus islas eran territorio  libre. Isla Tortuga fue una de las Islas mas famosas, donde los piratas  se reunían. Allí se formaban nuevas tripulaciones o se descansaba entre  viaje y viaje.

En un mundo, el del siglo XVII, claramente jerarquizada, racista y  sexista, los piratas eran una de las sociedades mas avanzadas de la  época, que no quería pertenecer a la Sociedad.
