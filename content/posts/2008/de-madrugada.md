---
date: 2008-05-29 23:22:39+00:00
title: de madrugada
tags:
- momentos
- strasbourg
- spanish
---

{{< figure src="/img/2008/estrasburgo.jpg" alt="estrasburgo" class="right" >}}
Las 6 de la mañana, me despierto en un tren parado. Todo lo que se ve  por la ventanilla es blancura, una espesa niebla que lo cubre todo.  Cuando me quede dormido estaba en Alemania tratando de llegar a Francia.  Tantos días de viaje, tantos trenes que me han llevado, no se ni donde  estoy, y el echo de que este medio dormido no ayuda a quitar esa  sensación de desorientación.

La gente a mi alrededor, entre bostezos, recoge su equipaje. Todo el  mundo con cara somnolienta y en silencio esta bajando del tren. Parece  que esta es la última parada de este tren. Yo también desciendo del  tren, me ajusto la mochila, y me preparo para ver que me depara esta  nueva parada.

Al salir al exterior el frío glaciar aleja la somnolencia de mi, pero  la niebla persistente ayuda a que la confusión permanezca. Esto parece  seguir siendo Alemania. Casas de dos o tres pisos, con las vigas de  madera a la vista y salientes que, en las estrechas calles, hacen  difícil ver el cielo. Bares con buena cerveza. Carnicerías con  salchichas. Las panaderías tienen cruasanes y otras muchas delicias  francesas mezcladas con los típicos bollos y panes bábaros.

Estoy en **Estrasburgo**, una ciudad en la frontera  entre Francia y Alemania, en la que me encuentro con una mezcla de las  dos culturas. Pasear por el centro es agradable, un enjambre de calles  estrechas circundadas por un río Rin.

Pero no me puedo quedar mucho aquí, mi objetivo era ir a Lyon, donde  tengo amigos esperándome. Así que de vuelta en la estación tras un par  de horas de paseo me monto en un tren para continuar mi viaje.
