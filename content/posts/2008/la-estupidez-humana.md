---
date: 2008-05-24 23:25:37+00:00
title: La estupidez humana
toc: true
tags:
- quote
- spanish
---

Las **Cinco Leyes de la Estupidez**, según Carlo Cipolla:

> ## Primera Ley
> Siempre subestimamos el número de gente estúpida.
> 
> ## Segunda Ley
> La probabilidad de que una persona sea estúpida es independiente de cualquier otra característica de la persona.
> 
> ## Tercera Ley (la de Oro)
> Una persona estúpida es alguien que ocasiona daño a otra persona, o a un  grupo de gentes, sin conseguir ventajas para ella misma –o aun  resultando dañada.
> 
> ## Cuarta Ley
> La gente no estúpida siempre subestima el poder de causar daño de la  gente estúpida. Constantemente se les olvida que en cualquier momento, y  bajo cualquier circunstancia, el asociarse con gente estúpida  invariablemente constituye un error costoso.
> 
> ## Quinta Ley
> Una persona estúpida es la persona más peligrosa que puede existir.

A las que siguen los colorários de Livraghi:

> ## Primer Corolario:
> En cada uno de nosotros hay un factor de estupidez, el cual siempre es más grande de lo que suponemos.
> 
> ## Segundo Corolario:
> Cuando la estupidez de una persona se combina con la estupidez de otras,  el impacto crece de manera geométrica –es decir, por multiplicación, no  adición, de los factores individuales de estupidez.
> 
> ## Tercer Corolario:
> La combinación de la inteligencia en diferentes personas tiene menos  impacto que la combinación de la estupidez, porque (Cuarta Ley de  Cipolla) ” la gente no estúpida tiende siempre a subestimar el poder de  daño que tiene la gente estúpida”.

[El Poder de la Estupidez](http://biblioweb.sindominio.net/s/view.php?CATEGORY2=2&ID=117) – Livraghi, Giancarlo
