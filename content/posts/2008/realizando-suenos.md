---
date: 2008-05-20 14:55:48+00:00
title: realizando sueños
tags:
- momentos
- spanish
- squat
- rasmia
---

Una pared de pladur, sobre ella un  mural de carteles recopilados durante años que nos muestran una Zaragoza  viva y en lucha. Al mirarlo vemos la historia de gentes que piensan y  se expresan de forma diferente. La melancolía nos acoge recordando  aquellos momentos de lucha vividos.

A su alrededor fluye una  efervescencia de vida y actividad, como tratando de competir con este  pasado. Debates de gente discutiendo y creando ideas. Un bullir de  actividades, desde talleres y cursos donde aprender y repensar, hasta  visionado de películas y obras de teatro, donde disfrutar y relajarse.

Dicen que el local llevaba años sin  ser usado, que el dueño nunca iba por allí y que los vecinos no  conseguían localizarlo. Dicen que un grupo de jóvenes del barrio,  cansados de no tener espacios para sus actividades lo reconstruyeron y  le dieron vida. Dicen que desde entonces el barrio a cambiado, ahora hay  conciertos en espacios que ni el ayuntamiento se acordaba que tenía y  pasacalles que reúnen a todas las generaciones que pueblan el barrio.  Dicen que lo llamaron Rasmia en honor a la iniciativa y energía que  siempre han tenido las gentes de esta zona.

Ahora van a venir a cerrar por la  fuerza este espacio de sueños y vivencias. El viernes 30 de Marzo  pretenden terminar con este proyecto rebelde y creativo. Aunque lo  cierren, Rasmia nunca desparecerá para los que lo hemos vivido. Siempre  estará en nuestros recuerdos.
