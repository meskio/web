---
date: 2008-05-15 15:06:04+00:00
title: Una crítica a las patentes
toc: true
tags:
- spanish
- patents
---

{{< figure src="/img/2008/patented.jpg" alt="patented by Inartistic" class="right" >}}
Un a patente es un derecho de uso en  exclusiva (monopolio) sobre una invención. La patente se solicita a una  oficina de patentes y si esta cumple los requisitos para ser patentable  la oficina de patentes la adjudica, permitiendo el monopolio sobre esta  invención durante 20 años.

Actualmente tenemos una imagen de las  patentes como una herramienta para ayudar a la innovación. Por un lado  se favorece ala investigación otorgando un monopolio sobre el invento,  permitiendo a las empresas recuperar la inversión realizada en  investigación. Mientras que por otro lado se fuerza a que las empresas  publiquen sus investigaciones, pues para recibir una patente hay que  entregar la especificación de la innovación a la oficina de patentes, la  que al expirar la patente pone a libre disposición esta información.

Pero, ¿todo es tan *bonito*?


## Un poco de historia


Se cree que el origen de las patentes  pudo ser la antigua Grecia o la Italia de las ciudades estado. Pero del  primer sistema de patentes del que se tiene constancia es de 1449,  cuando Enrique VI de Inglaterra creó el sistema de patentes para dar el  monopolio sobre un producto a los amigos de la corona o al que pagara  por el. Para recibir este monopolio no hacia falta haber inventado el  producto, ni que este fuera innovador. Esto provocó muchos abusos por  parte de la corona, que concedía patentes sobre  cualquier tipo de  vienes. Por ejemplo llego a conceder patentes sobre la sal.

Esta situación no cambió hasta el  siglo XVII, en el que el parlamento, envuelto en cambios sociales,  obligó a la corona a modificar las patentes. Esta vio que si perdía las  patentes perdería una gran fuente de ingresos, por lo que para  protegerse y poder seguir concediendo patentes cambió el discurso.  Empezó a hablar de la necesidad de promocionar la innovación y que un  sistema de monopolios (el sistema de patentes) podía ayudar a ello. Con  ello consiguió que se mantuviera el sistema de patentes exigiendo que  fuera una invención nueva para poder recibir la patente.

A lo largo del siglo XIX este modelo  se fue esparciendo por toda Europa, y la mayoría de los países europeos  (y también algunos no europeos) adoptaron sistemas de patentes parecidos  al Ingles. El sistema de patentes fue evolucionando, con la hegemonía  EEUU este paso a tomar las riendas de el y a forzar al resto del mundo a  adoptar sus decisiones. En 1977 entró en funcionamiento el Convenio de  Patente Europea que unifica el sistema de patentes en toda Europa, con  una única oficina de patentes para todos los estados integrados en el.


## Situación actual de las patentes


En  la actualidad registrar una patente cuesta varios miles de euros, por  lo que las patentes no las pueden registran los inventores ni las  pequeñas empresas, sino las grandes empresas. La gran mayoría de las  patentes esta en manos de corporaciones (empresas multinacionales).  Estas se las intercambian entre ellas, de tal forma que una corporación  puede usar lo patentado por otras a cambio de que ella misma no demande a  las otras por usar sus patentes. Así se consigue un estado en el que  entre corporaciones el sistema de patentes realmente no se aplica, pero  cuando una empresa pequeña trata de entrar en el mercado es demandada  por violar las patentes de las corporaciones.

Las oficinas de patentes cada vez son  mas flexibles en cuanto a que permiten patentar, por lo que actualmente  se permite patentar, entre otras cosas, [la vida](http://igualdadsimios.blogspot.com/2007/02/patentes-sobre-la-vida.html).  Se permite patentar cosas como el genoma humano o plantas y animales  existentes. Muchas corporaciones (como Monsanto o Bayer) se dedican a ir  a tribus indígenas o campesinos, observar que plantas usan y que  propiedades tienen y patentan estas plantas como si fueran invento suyo.  Esta practica es conocida como [Biopiratería](http://www.nuestraamerica.info/leer.hlvs/3412).

Todas estas patentes sobre la vida se están usando para generar negocio aun a costa de vidas. Un gran ejemplo lo [tenemos en Irak](http://www.grain.org/articles/?id=7),  donde tras la entrada del ejercito EEUU entraron las corporaciones como  Monsanto, Syngenta, Bayer o Dow Chemical. Estas son las dueñas de la  patente sobre muchas de las plantas de cultivo usadas tradicionalmente  por los campesinos Iraquíes. Ellos siempre habían guardado las semillas  de una temporada para plantarlas en la siguiente temporada. Actualmente  con la nueva ley de patentes iraquí es ilegal que guarden las semillas  de un año a otro y tienen que volver a comprarlas a los *dueños* de la planta. Pero Irak no es un echo aislado, este tipo de situaciones se esta repitiendo por todo el mundo.

Otra de los tipos de patentes que producen grandes beneficios a unas pocas corporaciones son los medicamentos. Con las [patentes sobre medicamentos ](http://www.rebelion.org/noticia.php?id=24426)están  jugando con la vida de millones de persona. Con estas patentes en la  mano las farmacéuticas persiguen a los países del tercer mundo que se  atreven a producir medicamentos genéricos (a bajo coste), pues no les  han pagado por ello. Esto lo hemos visto en sitios como la [India](http://ciudaddelviento.blogspot.com/2007/09/resolucin-caso-novartis-vs-la-india.html),  Novartis había demandado a la India por fabricar medicamentos de bajo  coste violando sus leyes de patentes, por suerte se ha conseguido seguir  produciendo medicamentos.

Las empresas farmacéuticas dicen que  la investigación de medicamentos es muy cara y que sin las patentes no  podrían recuperar la inversión. Pero según  el *Informe 2006 de la Comisión sobre Salud Pública, Innovación y Derechos de Propiedad Intelectual* de la OMS, “entre 1995 y 2002 la industria farmacéutica fue la más  rentable de Estados Unidos, en términos de beneficio neto medio después  de impuestos como porcentaje de los ingresos. El 2003 decayó un poco (…)  pero mantuvo un  		margen de rentabilidad del 14%, tres veces superior a  la media de todas  		las empresas incluidas aquel año en la lista  Fortune 500″.

Últimamente en la Unión Europea hemos visto un gran debate sobre algunas [modificaciones a la ley de patentes europea](http://www.nosoftwarepatents.com/es/m/intro/index.html) que pretendían hacer instigadas por las corporaciones Estadounidenses, estas modificaciones pretendían legalizar las llamadas *patentes de software*.  Este tipo de patentes, principalmente aplicadas a la informática,  sirven para patentar ideas. En Estados Unidos, que las tienen  legalizadas, tienen patentes tan absurdas como el doble click o el poner  el botón de “aceptar”abajo a la derecha de las ventanas.


## Conclusiones


Los defensores de las patentes dirían  que los ejemplos expuestos en este articulo son problemáticos, pero  esto no significa que el sistema de patentes sea problemático de raíz.  Siempre se puede pedir a una oficina de patentes que retire una patente  dando razones de necesidad publica o de que no es un invento original,  pero este proceso es complejo y caro, muy pocas veces llega a buen  puerto. Parafraseando a Stallman, eliminar las patentes peligrosas de  una en una no solucionará el problema, como no lo hará el ir matando  mosquitos de uno a uno para eliminar la malaria.

Las patentes están siendo una gran  herramienta para que las corporaciones sigan controlando el mercado e  impedir la entrada al mercado a nuevas empresas. Ellas son las únicas  que pueden crear productos y se relacionan entre ellas para conseguir  este objetivo. A través de este método pueden controlar que hacen los  gobiernos y empresas, y decidir que productos pueden o no producir.

El sistema de patentes estaba  corrupto desde que se creo y lo sigue estando en la actualidad.  Contrariamente a la imagen de liberación y promoción del conocimiento,  sirve para beneficiar a unos pocos generando grandes desigualdades  sociales, frenando el uso e invención de conocimiento a la sociedad.
