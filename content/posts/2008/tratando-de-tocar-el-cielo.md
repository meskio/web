---
date: 2008-05-18 15:04:46+00:00
title: Tratando de tocar el cielo
tags:
- momentos
- spanish
- mountain
---

{{< figure src="/img/2008/as-night-falls.jpg" alt="as night falls by crystaleyes909" class="right" >}}
Una noche cualquiera, de invierno,  2500 metros de altura, un refugio compuesto por cuatro paredes, un techo  y una chimenea humeante. Dos personas se calientan las manos con el te  hirviendo de sus potos, mientras miran maravillados el paisaje.

Todo esta quieto, en silencio, solo  roto por el crepitar de un puchero con nieve deshelándose y el viento  ululando entre las montañas. La luna esta apareciendo sobre una cresta  iluminando el valle. Todo esta blanco, brillante, a excepción de los  escarpados en los que la nieve no puede posarse y se vislumbra la roca  oscura.

Un lago congelado, laderas nevadas en  las que se vislumbra algún sarrio perdido como una mancha oscura. La  vegetación inexistente y las crestas afiladas crean un ambiente   inhóspito recordando que en un tiempo pasado ningún humano se acercaría a  estos parajes.

Una conversación en voz baja, como  intentando preservar la calma que reina. Entre sorbos de te hablan  del  día que les espera mañana. Relajados, con el cuerpo cansado tras un  largo día de actividad, pero expectantes por la ascensión que les espera  al día siguiente.

Los pocos animales que hay, mientras  descienden a zonas mas bajas donde hay mas comida, parecen preguntarse  que habrá llevado a estos humanos a venir a una región inhóspita.  Seguramente estos mismos humanos no tienen respuesta para esta pregunta,  simplemente sentían que tenían que subir allí.
