---
date: 2008-03-12 15:11:26+00:00
title: Mochilero y friki
tags:
- momentos
- spanish
---

{{< figure src="/img/2008/the-backpacker.jpg" alt="the backpacker by er0k" class="right" >}}
El sol se estaba poniendo, me encontraba en un país desconocido del que no hablaba el idioma. En una ciudad preciosa, pero demasiado grande. La había recorrido junto con mi mochila durante horas, sin encontrar ningún sitio donde dormir. Las calles me parecían todas iguales, las gentes se estaban recogiendo en sus casas y ya no había casi transeúntes.

Cansado, perdido, sin saber que hacer para pasar la noche llegué a una pequeña plaza. No parecía especialmente acogedora, era de cemento, con el suelo rajado por lo años, con un par de arboles raquíticos de los que parecía que nadie se había preocupado en años. Los edificios alrededor eran altos bloques de pisos, en los que se atinaba a ver luces de cocinas con gente cenando. Había un banco de madera, descolorido y con pintadas, pero parecía cómodo.

Me senté en el, saqué mi portátil. Algún vecino de la zona había dejado su red wifi abierta, puede que por descuido o puede que por tener ganas de compartir, el echo es que había dejado una puerta abierta al mundo. Me conecté a ella. Había sido un largo día, no sabia que iba a hacer después, pero en ese momento me sentí como en casa.
