---
date: 2008-11-21 22:58:06+00:00
title: El derecho a leer
tags:
- quotes
- spanish
- free software
---

Con las cosas que estamos viendo últimamente me he acordado de un texto que escribió RMS allá por el ‘96:

> En  sus clases de programación Dan había aprendido que cada libro tenía un  control de copyright que informaba de cuándo y dónde fue leído, y quién  lo leía, a la oficina central de licencias (usaban esa información para  descubrir piratas, pero también para vender perfiles personales a otras  compañías). La próxima vez que su ordenador se conectase a la red, la  oficina central de licencias lo descubriría. Él, como propietario del  ordenador, recibiría el castigo más duro, por no tomar las medidas  adecuadas para evitar el delito.

> Más tarde, Dan descubrió que hubo un tiempo en el que  todo el mundo podía ir a una biblioteca y leer artículos, incluso  libros, sin tener que pagar. Había investigadores que podían leer miles  de páginas sin necesidad de becas de biblioteca. Pero desde los años 90  del siglo anterior, tanto las editoriales comerciales, como las no  comerciales, habían empezado a cobrar por el acceso a los artículos. En  el 2047, las bibliotecas de acceso público eran sólo un vago recuerdo.

**[El derecho a leer](http://www.gnu.org/philosophy/right-to-read.es.html) – **[**Richard Stallman**](http://www.stallman.org/)














