---
title: "Utopía No Es Una Isla"
date: 2021-02-12T10:12:18+01:00
tags: 
  - spanish
  - review
  - book
  - utopia
  - comunism
  - marxism
  - ecosocialism
---

{{< figure src="/img/2021/utopia-no-es-una-isla.jpg" alt="utopía no es una isla" class="right" >}}

Layla Martínez plantea un recorrido histórico sobre los sueños de sociedades 
mejores en [Utopía no es una isla: Catálogo de mundos 
mejores](https://episkaia.org/producto/utopia-no-es-una-isla/). Empieza con un 
análisis muy interesante de como en las últimas décadas la gran mayoría de la 
producción cultural es distópica, y los problemas que eso conlleva:

> Los productos culturales reflejan la realidad, pero al hacerlo, también la 
> crean. Imaginar futuros peores nos ha quitado la capacidad de pensar en un 
> porvenir mejor.

El libro empieza con Tomas Moro y su Utopía, pasa por la era dorada de la 
piratería, para centrarse en el marxismo y sus herencias como el maoismo. Viendo 
las revoluciones desde la Rusa a Venezuela como sueños utópicos fallidos que 
planteaban una sociedad autoritaria temporal para llegar a modelos donde el 
pueblo participe mas directamente en la toma de decisiones. Relata una gran 
diversidad de propuestas revolucionarias, pero parece que para ella el 
anarquismo no existe o no merece la pena considerar sus utopías. Lo mas cercano 
a un movimiento antiautoritario que relata en el libro es Rojava.

Para terminar le dedica una buena parte del libro al ecologismo como alternativa 
a la visón distópica que empezamos a tener todas presente con el cambio 
climático. Tras criticar el ecofascismo, como un problema de racismo y 
nacionalismo, pasa muy de puntillas por el decrecimiento para centrarse en el 
ecosocialismo. Se explaya en relatarnos futuros de gobiernos centralizados, con 
economía planificada y donde la tecnología soluciona nuestros problemas 
ecológicos. No se si me siento mas incomodo con el hecho de que sueñe con 
estados férreos omnipresentes o con la visión de necesitar seguir destruyendo el 
planeta para generar tecnología que nos salve.

