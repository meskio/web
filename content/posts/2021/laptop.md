---
title: "Buying a Laptop"
date: 2021-06-03T09:54:13+02:00
toc: false
tags: 
  - english
  - laptop
  - review
  - technology
---

{{< figure src="/img/2021/coreboot.jpg" alt="flashing coreboot in my x230" class="right" >}}

My laptop died some weeks ago. It was a second hand lenovo x230 that I got to 
replace my previous x240. I know x240 is newer than x230, but the x230 can carry 
16GB of ram and is really nice to dismantle and replace parts. I think it was a 
great improvement.

After many years reusing old laptops or even using an eepc as my main computer 
I'm now looking into having a new powerful machine. I use 
[Qubes](https://www.qubes-os.org/) and tend to carry my laptop around with me 
all the time, because of that my two main requirements on a laptop are tons of 
RAM and being lightweight. Looking for laptops I've set up as limits to have 
least 32GB of ram and less than 1.5kg.


## AMD laptops

There is a lot of hype now with AMD Ryzen CPUs for laptops, and with good 
reasons. It's way more powerful than the counterpart Intel ones, the high end 
ones come with 8 cores (while Intel do 4 cores). They are pretty tempting.

But I have some few concerns about them. They don't seem to support USB-C video 
output, or at least you need special customization on linux to get it work. They 
are not so well supported by [Qubes 
yet](https://github.com/QubesOS/qubes-issues/issues?q=AMD+Ryzen+is%3Aissue+is%3Aopen).
And feature of Intel integrated GPUs is GVT-g (graphics virtualization), that I 
might want one day to enable it in Qubes.

Because of that I have decided to look into Intel laptops, anyway in the last 
years my main bottleneck hasn't being CPU but RAM.


## rebranded laptops

The first laptops that caught my attention are 
[System76](https://system76.com/), specially the [Lemur 
Pro](https://system76.com/laptops/lemur). System76 does have nice [hardware 
manuals](https://tech-docs.system76.com/) on how to replace most parts and come 
with coreboot and Intel ME disabled. They basically rebrand chinese laptops and 
many other shellers provide the same ones, for example the German company Tuxedo 
has the [InfinityBook S 
14](https://www.tuxedocomputers.com/en/Linux-Hardware/Linux-Notebooks/10-14-inch/TUXEDO-InfinityBook-S-14-v6.tuxedo), 
andI belive is the same hardware than the lemur pro.

Another interesting option are the [Slimbooks](https://slimbook.es/), another 
rebrander company, this time from spain. For me is an interesting option to get 
a laptop from spain, I would expect to have less problems to get tech support if 
something break than if I get it from the US (like the System76). Slimbook has a 
[kde laptop](https://kde.slimbook.es/) that is basically the same than their 
[PorX](https://slimbook.es/prox) or the [Tuxedo Pulse 
14](https://www.tuxedocomputers.com/en/Linux-Hardware/Linux-Notebooks/10-14-inch/TUXEDO-Pulse-14-Gen1.tuxedo).

The slimbook are nice because they have an ethernet wich the lemur pro lacks.  
But their keyboard look pretty bad and the [reviews 
around](https://jonathancarter.org/2020/09/13/wootbook-tongfang-laptop) seem to 
agree that the keyboard is not really good. I kind of like more the lemur pro, 
but I'm afraid of it not being robust enough to survive many years with the hard 
life I give to my laptops.


## discarded options

An option is purism laptops, but good friends [*recommend to avoid 
them*](https://anarc.at/blog/2020-07-13-not-recommending-purism/). They have 
[lied to their 
customers](https://www.phoronix.com/scan.php?page=news_item&px=Zlatan-Todoric-Interview), 
[have questionable politics](https://anarc.at/blog/2019-05-13-free-speech/) and 
do rely heavily on libre-washing. I decided not explore them much.

Another interesting one is the [framework](https://frame.work/), but is on the 
making and I wanted a laptop soon. I hope they do manage to deliver all that 
they are promising on a upgradable, repairable laptop.


## traditional laptop makers

Then I'm left with the *traditional* laptop makers, like lenovo, dell or hp.

The first thing I found out looking at them is that there is almost no laptop 
under 1.5kg with non-soldered ram. In the lenovo Thinkpads there is not a single 
model. Same for HP I have failed to find a single model. Dell does have at least 
one, the [Latitude 
5420](https://www.dell.com/en-us/work/shop/cty/pdp/spd/latitude-5420-laptop). A 
pretty nice laptop, with two slots for ram and ethernet port. Is a bit bigger 
than my previous laptops but not huge and just a bit heavier.

I failed to find any interesting hp laptop, not sure if I got lost on their 
website or the just don't have any laptop I might like.

Accepting the reality of soldered ram and seeing that I can go lighter than my 
previous laptops I end up eyeing two options:
* [dell XPS 
  13](https://www.dell.com/en-us/work/shop/dell-laptops-and-notebooks/new-xps-13-laptop/spd/xps-13-9310-laptop).  
Smaller size than the x230/x240 with a bigger screen. Has a *developer option* 
with ubuntu preinstalled (not available in the spanish shop). But the only 
connectors are two USB-C, one microSD and a minijack. I guess I can live without 
ethernet, but that looks a bit too limited.
* [lenovo X1 
  Carbon](https://www.lenovo.com/us/en/laptops/thinkpad/thinkpad-x1/X1-Carbon-G9/p/22TP2X1X1C9).  
A bit bigger than the XPS 13 or my previous laptops, but lighter than any of 
them. With most of the connectors I would want, I just miss the ethernet. A  
thinkpad keyboard, which I think I like more than the dell ones. And probably 
more robust than the XPS (it says is *military-grade*). I guess the main 
drawback besides the soldered ram and the lack ethernet is that is not a really 
cheap laptop.


## the final decision

It took me several days, looking into laptops and thinking under the shower. But 
finally I decided to accept the lack of repairability in exchange of a light and 
hopefully a more robust laptop.

My pick has being the Lenovo Thinkpad X1 Carbon Gen 9. I should receive it in 
some weeks, I guess the chip shortage is affecting here. Let's see how it goes 
then.
