---
date: 2013-03-11 10:12:02+00:00
title: Manifiesto Cypherpunk
tags:
- english
- quotes
- surveillance
- cryptography
- cypherpunk
- hacker
---

{{< figure src="/img/2013/state-of-surveillance.jpg" alt="state of surveillance by xp0s3" class="right" >}}
> We the Cypherpunks are dedicated to building anonymous systems. We are defending our privacy with cryptography, with anonymous mail forwarding systems, with digital signatures, and with electronic money.
> 
> **Cypherpunks write code**. We know that someone has to write software to defend privacy, and since we can't get privacy unless we all do, we're going to write it. We publish our code so that our fellow Cypherpunks may practice and play with it. Our code is free for all to use, worldwide. We don't much care if you don't approve of the software we write. We know that software can't be destroyed and that a widely dispersed system can't be shut down.

[A Cypherpunk Manifesto](http://www.activism.net/cypherpunk/manifesto.html) - Eric Hughes
