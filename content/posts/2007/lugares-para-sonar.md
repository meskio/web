---
date: 2007-09-11 22:35:10+00:00
title: Lugares para soñar
tags:
- momentos
- spanish
- oxford
- museum
- sherwood
---

Una sala amplia, con iluminación  leve, pasillos estrechos entre vitrinas repletas de objetos que se  amontonan unos encima de otros. Cada objeto en el que se posa la vista  revela historias y formas de vivir de culturas lejanas. Bajo las  vitrinas cientos de cajones esconden mas reliquias que aparecen al abrir  aleatoriamente alguno de ellos.

Es el museo Pitt Rivers en Oxford,  en el que se encuentra una mezcla de objetos que parece no tener  sentido, pero que en global trasmite  la diversidad de culturas de la  humanidad. Entre el caos aparecen objetos de lo mas diverso, como  cabezas reducidas  por los indios del Amazonas, sarcófagos de momias o  una botella de CocaCola que con dos tubos se ha convertido en cachimba  para fumar marihuana.

A los pocos visitantes se les ve  inclinándose sobre las vitrinas, iluminando los objetos con linternas y  esforzándose para poder leer las inscripciones escritas a mano que, en  pequeños papeles, cuelgan de los objetos.

El museo transmite sensaciones y  vivencias de gentes que, aunque no llegue a comprender del todo, me   sorprenden y causan admiración.

## 8/9/07

Sentado en una enorme raíz de uno de los muchos robles centenarios que componen el bosque de Sherwood como únicos sonidos a mi alrededor: ramas de arboles que se agitan al  viento y algún que otro animal que se mueve a hurtadillas. Cientos de  estrechos caminos que se entrecruzan me han guiado, y perdido, hasta  llegar a este remanso de paz.

Los helechos pueblan el suelo  haciendo imposible ver a las ardillas y otros animales que se mueven  bajo ellos. El bosque de hayas se cierra impidiéndome ver mas allá de 10  metros, con robles surgiendo por todas partes.

Es fácil imaginar a los druidas que  recorrieron este bosque recogiendo plantas mágicas de entre los robles, y  los bandidos que, como Robin Hood,  se escondían entre la espesura.
