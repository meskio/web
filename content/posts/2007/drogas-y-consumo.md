---
date: 2007-07-26 22:53:55+00:00
title: drogas y consumo
tags:
- spanish
- drugs
---

{{< figure src="/img/2007/roger-tallada.jpg" alt="drogas" class="right" >}}
Muchos debates se plantean sobre si  legalización o no de las drogas. Si que drogas son buenas y cuales no.  Si consumir unas drogas, otras o ninguna. El primer punto que hay tener  en cuenta para debatir es que la distinción entre drogas legales (alcohol, tabaco, café, …) e ilegales es completamente artificial. Por  que sean ilegales no son peores o mejores que las legales, cada droga  tiene sus características.

Nos encanta mirar mal al chaval que  se mete una raya o se fuma un porro. Mientras que media sociedad es  adicta a la cafeína. ¿Cuanta gente no puede afrontar el día sin tomarse  un café por la mañana?

Respeto, y admiro, a los que han  decidido dejar de tomar drogas. Pero no creo que todo el mundo deba  dejar de tomar drogas. Creo que las drogas como forma de ocio o  utilitarias tienen su función y que cada persona debe decidir cuales  toma y cuales no. En lo que tenemos que trabajar es en la educación, que  haya información veraz sobre los efectos de las drogas. No me sirve que  me digas si tomas drogas te vas a morir o acabaras en un manicomio.  Este tipo de advertencias no funcionan, pues meten todas las drogas en  el mismo saco como si todas tuvieran los mismos efectos.

Las drogas siempre han estado  presentes en todas las sociedades humanas, cada una a tratado las drogas  de una forma diferente según su cultura. El problema que veo es el uso  actual de las drogas, se ha convertido en algo normal consumir drogas  sin buscar unos efectos de ellas, sino simplemente como costumbre. Poco a  poco estamos perdiendo la capacidad de disfrutar de las drogas, pues  las tomamos todas indistintamente sin intentar aprovechar los efectos  beneficiosos de unas o de otras.

No todas las drogas producen los  mismos efectos ni se pueden consumir en los mismos ambientes. Usamos de  forma recreativa para salir de marcha todo tipo de drogas, cuando los  efectos de ciertas drogas (como el cannabis o ciertos alucinógenos) no  son apropiados para esto.

Por otro lado esta el abuso de las  mismas. Es difícil poner un limite entre lo que es uso y lo que es  abuso, y no pretendo ponerlo. Lo único que veo es cuando disfruto con  una persona y cuando no. Si por causa de los efectos de las drogas  pierdo el interés de relacionarme con cierta persona este es su  problema, se lo diré y le ayudaré si trata de cambiar, pero le indicaré  que si sigue con este consumo me perderá a mi.

Ni drogas si ni drogas no. Drogas con  cabeza, que ya somos mayores y no necesitamos que el “papa estado” nos  diga lo que tenemos que hacer.
