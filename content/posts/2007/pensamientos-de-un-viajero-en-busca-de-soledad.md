---
date: 2007-08-20 21:46:03+00:00
title: Pensamientos de un viajero en busca de soledad
tags:
- spanish
- momentos
---

Estaba sentado en el cementerio,  cansado de tanto andar. Algunos turistas perdidos pasaban preguntándose  que les había traído allí. Muchos de ellos hablaban en español, pero yo  no me sentía con ganas de descubrirme como hispanohablante. A veces te  encuentras mejor en el anonimato, con la gente pensando que tu no hablas  su idioma.

Mi mochila reposaba en la consigna de la estación, esperando a que me decidiera si quería pasar la noche aqui o continuar viaje.

Era París, la había  disfrutado profundamente escapando de los sitios turísticos y  recorriendo sus calles, escuchando a sus gentes hablar un idioma que no  entendía y observando la vida en esta gran ciudad, que a mi, como  espectador, me resultaba armoniosa y tranquila.

## 03/08/07

Bélgica,  nuevo país, nuevo idioma. Si me quejaba del francés,  el flamenco es  peor, no se entiende ni una palabra y suena como si fueran a escupir.

Pero por lo menos estoy en Gante (Gent), una ciudad de la que tengo muy buenos recuerdos de cuando la  visité con mis padres de pequeño. Nada mas llegar veo por que me gustaba  tanto, canales, casas de ensueño. Es una ciudad muy tranquila y  agradable.

Pero no todo tenía que ser tan  bonito. Llego al único albergue de la ciudad y esta lleno. Me  recomiendan hoteles, pero todos se salen de mi presupuesto. Así que toca  volver al viejo estilo de viajar, a dormir en la calle.

Por lo menos he encontrado una  pradera tranquilo en la que parece que pasaré una buena noche. El tiempo  es perfecto, tengo una cerveza, mi mochila y todavía me queda ilusión.

## 06/08/07

Las 11 de la noche, a mi alrededor una explanada de parking desierta. Es el décimo de un edificio en el centro de Bruselas (Bruxelles). Desde aquí se puede ver toda la ciudad iluminada sin el bullicio de turistas alrededor.

El atomium a un lado, la Gran Place  al otro, la catedral, … Todo se ve diferente cuando estas solo, cuando  no oyes el ruido de la gente a tu alrededor.

Los coches y la gente se ven pequeños ahí abajo, manteniendo su actividad ajenos  a que hay sitios donde huir de ellos.

Siento soledad, pero es una sensación  que me esta empezando a gustar. Que pocas veces no tomamos tiempo para  encontrarnos con nosotros mismos. Siempre con mil actividades corriendo  de un lado para otro, y con miedo a parar, no vaya a ser que no nos  guste lo que encontramos en nuestro interior.

Calmado, disfrutando del momento, sin  prisa por ir a ningún sitio, sin hora a la que estar en ningún lado.  Simplemente estando ahí.

## 08/08/07

Una  calle estrecha, llena de curvas, con pasadizos debajo de arcos,  enredaderas cayendo de las paredes. Un banco apacible donde sentarse.

Es pleno centro de Amberes (Antwerpen), pero la entrada escondida hace que pocos turistas se aventuren a pasar por aquí.

Hay varios restaurantes pequeños y  hogareños con típica comida Belga. Edificios  del siglo XVI, paredes de  ladrillo rojo, ventanas con cruceta de piedra y rejas de metal y arcos  de piedra.

El suelo esta empedrado, las piedras  desgastadas por los siglos parecen querer contar la historia de las  gentes que las han pisado. Cada esquina recuerda los chismorreos que los  vecinos se intercambiaban.

Esto es Vlaeykensgang, donde vivían los mas pobres entre los pobres de Amberes.
