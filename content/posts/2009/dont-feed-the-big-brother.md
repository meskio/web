---
date: 2009-05-21 22:14:18+00:00
title: Don’t feed the big brother
tags:
- spanish
- surveillance
- google
---

{{< figure src="/img/2009/big-brother.jpg" alt="big brother" class="right" >}}
Imagina que alguien supiera todo lo que haces, qué te gusta, de qué  hablas con tus amigos, con quién te relacionas… y que no tuviera  información solo de ti, sino de una gran cantidad de personas. Imagina  que además tuviera la tecnología para relacionar toda esa información y  saber más sobre ti que tu mismo.

Imagina que eres tu quien le das toda esa información gustosamente,  pues a cambio te ofrece servicios que te resultarán muy atrayentes.  Imagina que su eslogan es “no soy malvado” y tu te lo creyeras, no  queriendo enterarte de que colabora con más malos ocultando que otros no  quieren que se sepa, al margen de las leyes de privacidad y o cualquier  otro control.

No imagines más, esa corporación existe y se llama google. Cada vez  que haces una búsqueda en google, ves o subes un video a youtube,  escribes un correo en gmail, posteas en blog de blogger, hablas por  gtalk, buscas una dirección en google maps o google earth, usas tu  google calendar o docs, subes una foto a picasa… le das información a  este gran hermano sobre ti.

Pero tu uso de google no solo te afecta a ti, sino también a los que  te rodean. Si tu web tiene publicidad con google adsense o generas  estadísticas de uso a través de google analytics, si tu cuenta de correo  es de gmail o si tu cuenta de jabber es de gtalk… nos obligas a los  demás a darle nuestra información al granhermano para poder comunicarnos  contigo.

No alimentes al gran hermano.
No uses google.
