---
date: 2010-08-19 22:18:13+00:00
title: hackeando trasnochado
tags:
- momentos
- spanish
- hack
---

{{< figure src="/img/2010/hacker-holmes.jpg" alt="Hacker Holmes by EranFowler" class="right" >}}
Desde hace ya casi diez meses que el sol marca mi horario. Despierto con  la alarma y voy a trabajar, como la gente de bien. Durante 8 horas al  día soy programador a sueldo, picando código que se usara en alguna  máquina que no acabo de entender para algún propósito que se me escapa.  No me quejo, no es un mal trabajo, buen ambiente, mucho friki a mi  alrededor, trabajar al ritmo que yo me marco; estoy aprendiendo mucho.  Por las tardes me suele tocar socializarme, a los amigos hay que  cuidarlos, asi que solo me quedan las noches libres para hackear en lo  que a mi me gusta.

Llego a casa normalmente hacia las diez u once de la noche, mis  compañeros de piso a esas horas ya están durmiendo. Con la casa en   silenciosa me siento en mi mesa. Frente a mi una ventana a traves de la  que se intuyen montañas, con puntitos de luz distribuidos al azar de  casas con alguien despierto,  y nuves, siempre nuves, creando formas de  diferentes tonalidades de azul oscuro, a menudo lloviendo, salpicando de  gotitas la ventana. A mi alrededor, una habitacion en penumbra, la luz  de la pantalla crea formas fantasmagoricas entre las pilas de libros y  papeles que pueblan la mesa y la cama, convinandose con los parpadeos  azules, verdes y rojos que emiten los diversos dispositivos, que como  tentaculos enmarañados se distribuyen por la mesa conectados al  ordenador. Un mate en la mano, me calienta las manos y empaña las gafas  si me acerco demasiado. Del ordenador sale alguna musica ambiental, que  se mezcla con el sonido del tecleo y del mate. Estas sesiones suelen  durar hasta las 2 o las 3 de la mañana, cuando cayendome de sueño me  arrastro hasta la cama para dormir mis 5 o 6 horas hasta que el  despertador marque el comienzo de un nuevo ciclo.

Ya hace unos tres años que empece [tudu](https://code.meskio.net/tudu/),  una herramienta que yo queria para mi y una escusa para aprender C++.  En estos años de dedicacion esporadica ha ido creciendo, heredando toda  mi inexperiencia. Cada poco me toca refactorizar alguna pequeña parte,  para que el codigo tenga mas coherencia y se pueda añadir la nueva  caracteristica con la que me estoy peleando. Durante las ultimas noches  he estado metido de lleno una de estas refactorizaciones, intentando que  una parte del programa hiciera lo mismo que ya hacia, pero con un  codigo mas claro, limpio y estructurado. Puede ser un poco deprimente,  dedicarle horas a escribir codigo que no hace nada nuevo, pero si  consigo que el nuevo codigo sea mas corto y legible me doy por  realizado.
