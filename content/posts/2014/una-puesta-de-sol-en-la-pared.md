---
date: 2014-05-29 23:28:44+00:00
title: Una puesta de sol en la pared
tags:
- spanish
- momentos
- mountain
---

{{< figure src="/img/2014/punta-italia.jpg" alt="punta italia" class="right" >}}
El sol se ponía entre las escarpadas montañas frente a ellos. Los rayos rojizos se reflejaban en las grietas del glaciar bajo ellos, revelando miles de tonalidades de azul en el hielo. Todo un espectáculo de luz y color. Sus miradas permanecían clavadas en el horizonte, pero sus mentes vagaban mas allá.

Uno de ellos recordaba momentos antes del amanecer: su compañero escalando encima suyo, la luz de las linternas, el frío de la noche, las manos entumecidas, doloridas, los pies insensibles desde hacía muchas horas. En esa pequeña repisa de hielo de unos pocos centímetros, él no paraba de moverse intentado sin éxito recuperar el calor. Recoge una gaza de cuerda, después otra, y otra, hasta tener toda la cuerda ordenada en la mano, tírala al suelo y vuelta a empezar, recoge una gaza de cuerda, otra mas, ...

Otro miraba al descenso: allá abajo un punto naranja marcaba su tienda, y la promesa de una sopa caliente. Quedan muchos rápeles, demasiados para lo cansados que están. La roca no parece muy estable, se rompe al tocarla, no va a ser fácil encontrar donde poner un clavo. Tras los rápeles, allí abajo, el glaciar y un camino a descubrir zigzagueante entre las grietas. Va a costar ese descenso.

El tercero intenta sin éxito encontrar una razón a lo que están haciendo. ¿Por que alguien decide ir a ascender una montaña? ¿Que sentido tiene tanto esfuerzo? ... Preguntas sin respuesta que se agolpan en su mente, pero sabe que cuando vuelva a la ciudad solo va a pensar en la siguiente montaña. Por nada del mundo cambiaría el estar ahí, compartiendo los últimos sorbos de un te ya no tan caliente contemplando esa puesta de sol.
